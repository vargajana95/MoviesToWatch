package vargajana95.moviestowatch;

import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;


public abstract class BaseDetailsActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.6f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;


    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    private LinearLayout mTitleContainer;
    private TextView mTitle;
    private TextView expandedTitle;
    private AppBarLayout mAppBarLayout;
    private CollapsingToolbarLayout toolbarLayout;
    private ImageView ivPoster;
    private ImageView ivBackDrop;

    private Guideline guideline;

    private int guidelineStartingWith;


    protected void bindActivity() {
        mTitle = findViewById(R.id.main_textview_title);
        mTitleContainer = findViewById(R.id.mainInfoContainer);
        mAppBarLayout =  findViewById(R.id.app_bar);
        toolbarLayout = findViewById(R.id.toolbar_layout);
        ivPoster = findViewById(R.id.ivPoster);
        ivBackDrop = findViewById(R.id.ivBackDrop);
        expandedTitle = findViewById(R.id.expandedTitle);
        guideline = findViewById(R.id.guideline);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button_seen pressed
                onBackPressed();
            }
        });

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }



        mAppBarLayout.addOnOffsetChangedListener(this);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        expandedTitle.setSelected(true);
        //setTitle(" ");

        startAlphaAnimation(mTitle, 0, View.INVISIBLE);

        guidelineStartingWith = ((ConstraintLayout.LayoutParams) guideline.getLayoutParams()).guideBegin;

    }

    protected void setCollapsingTitle(String title) {
        //toolbarLayout.setTitle(title);
        mTitle.setText(title);
        expandedTitle.setText(title);
    }

    protected void setPosterImage(String url) {
        Glide.with(this)
                .load(url)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_movie))
                .into(ivPoster);
    }

    protected void setBackdropImage(String url) {
        Glide.with(this)
                .load(url)
                //.apply(bitmapTransform(new BlurTransformation(25)))
                .apply(RequestOptions.errorOf(R.drawable.rect_shape))
                .into(ivBackDrop);
    }

    protected void displayChangingData() {
        invalidateOptionsMenu();
        displayInfo();
        displaySeenPanel();
    }

    protected abstract void displaySeenPanel();

    protected abstract void displayInfo();

    protected abstract void displayHeader();

        @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);

        handleInfoPanelChange(percentage);
    }

    private void handleInfoPanelChange(float percentage) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) guideline.getLayoutParams();
        params.guideBegin = guidelineStartingWith -(int) Math.floor(guidelineStartingWith*percentage);
        guideline.setLayoutParams(params);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}