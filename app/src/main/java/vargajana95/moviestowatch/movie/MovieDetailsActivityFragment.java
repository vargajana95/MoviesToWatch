package vargajana95.moviestowatch.movie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.MovieData;

/**
 * A placeholder fragment containing a simple view.
 */
public class MovieDetailsActivityFragment extends Fragment {
    private MovieDataProvider movieDataProvider;

    private TextView tvOverView;
    private TextView tvActors;
    private TextView tvDirector;
    private TextView tvRating;
    private TextView tvPersonalScore;
    private TextView tvTmdbLink;
    private TextView tvImdbLink;
    private LinearLayout personalScoreLayout;


    public MovieDetailsActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof MovieDataProvider) {
            movieDataProvider = (MovieDataProvider) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement MovieDataProvider interface!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        MovieData movieData = movieDataProvider.getMovieData();
        if (movieData != null)
            displayMovieData(movieData);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_details, container, false);

        tvOverView = view.findViewById(R.id.tvOverView);
        tvActors = view.findViewById(R.id.tvActors);
        tvDirector = view.findViewById(R.id.tvDirector);
        tvRating = view.findViewById(R.id.tvRating);

        tvPersonalScore = view.findViewById(R.id.tvPersonalScore);
        tvTmdbLink = view.findViewById(R.id.tvTmdbLink);
        tvImdbLink = view.findViewById(R.id.tvImdbLink);
        personalScoreLayout = view.findViewById(R.id.personalScoreLayout);


        /*MovieData movieData = movieDataProvider.getMovieData();
        if (movieData != null)
            displayMovieData(movieData);*/



        return view;
    }

    private void displayMovieData(MovieData movieData) {
        tvOverView.setText(movieData.overview);
        tvActors.setText(movieData.getTopActorsAsString(3));
        tvDirector.setText(movieData.getDirectorsAsString());
        tvRating.setText(getContext().getString(R.string.string_rating, movieData.vote_average));

        if (movieData.getPersonalRating() == 0 || movieData.getSeenStatus() != MovieData.SeenStatus.SEEN) {
            personalScoreLayout.setVisibility(View.GONE);
        }
        else {
            personalScoreLayout.setVisibility(View.VISIBLE);
            tvPersonalScore.setText(getContext().getString(R.string.string_rating, movieData.getPersonalRating()));
        }

        tvImdbLink.setText(movieData.getImdbLink());
        tvTmdbLink.setText(movieData.getTmdbLink());


    }

    public interface MovieDataProvider {
        MovieData getMovieData();
    }
}
