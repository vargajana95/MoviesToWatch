package vargajana95.moviestowatch.movie;

/**
 * Created by Varga János on 2018. 02. 23..
 */

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import vargajana95.moviestowatch.BaseDetailsActivity;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.RateDialog;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.MovieDataHolder;


public class MovieDetailsActivity extends BaseDetailsActivity implements MovieDetailsActivityFragment.MovieDataProvider {
    public static final String MOVIE_ID = "MOVIE_ID";

    private MovieData movieData;
    private TextView tvGenres;
    private TextView tvReleaseDate;
    private TextView tvRuntime;
    private TextView tvDate;
    private Button btnWatch;
    private LinearLayout dateView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);


        bindActivity();


        tvGenres = findViewById(R.id.tvGenres);
        tvReleaseDate = findViewById(R.id.tvReleaseDate);
        tvRuntime = findViewById(R.id.tvRuntime);
        tvDate = findViewById(R.id.tvWatchDate);
        btnWatch = findViewById(R.id.btnWatch);
        dateView = findViewById(R.id.dateView);


        tvGenres.setSelected(true);

        int movieID = getIntent().getExtras().getInt(MOVIE_ID, -1);

        //TODO else
        if (movieID != -1) {
            //Movie is not in the MovieDataHolder, need to get info from api
            MovieData movieToDisplay = MovieDataHolder.getInstance().getMovieById(movieID);
            if (movieToDisplay != null)
                displayMovieData(movieToDisplay);
            else {
                movieToDisplay = MovieDataHolder.getInstance().getFromMovieCacheById(movieID);
                if (movieToDisplay != null)
                    displayMovieData(movieToDisplay);
                else {
                    //loadAndDisplayMovieData(movieID);
                    finish();
                }
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        displaySeenPanel();
    }

    @Override
    protected void displayHeader() {
        setCollapsingTitle(movieData.title);

        setPosterImage("https://image.tmdb.org/t/p/w500" + movieData.poster_path);

        setBackdropImage("https://image.tmdb.org/t/p/w500" + movieData.backdrop_path);

        tvGenres.setText(movieData.getGenresAsString());
        tvReleaseDate.setText(movieData.release_date.split("-")[0]);
        tvRuntime.setText(getString(R.string.run_time, movieData.runtime));


    }

    @Override
    protected void displayInfo() {
        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment, MovieDetailsActivityFragment.class.newInstance(), "MovieDetailsActivityFragment").commit();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void displaySeenPanel() {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");

        if (movieData.getSeenStatus() == MovieData.SeenStatus.SEEN) {
            btnWatch.setBackground(getResources().getDrawable(R.drawable.button_seen));
            btnWatch.setText(R.string.seen_it);
            btnWatch.setTextColor(getResources().getColor(R.color.buton_seen));
            dateView.setVisibility(View.VISIBLE);
            tvDate.setText(getResources().getString(R.string.seen_date, format1.format(movieData.getSeenDate().getTime())));
        } else if (movieData.getSeenStatus() == MovieData.SeenStatus.NOT_ON_LIST) {
            btnWatch.setBackground(getResources().getDrawable(R.drawable.button_not_seen));
            btnWatch.setText(R.string.not_seen);
            btnWatch.setTextColor(getResources().getColor(R.color.button_not_seen));
            dateView.setVisibility(View.INVISIBLE);
        } else if (movieData.getSeenStatus() == MovieData.SeenStatus.NEED_TO_SEE) {
            btnWatch.setBackground(getResources().getDrawable(R.drawable.button_on_watchlist));
            btnWatch.setText(R.string.on_watchlist);
            btnWatch.setTextColor(getResources().getColor(R.color.button_on_watchlist));
            dateView.setVisibility(View.VISIBLE);
            tvDate.setText(getResources().getString(R.string.watchlist_date, format1.format(movieData.getOnWatchlistSince().getTime())));
        }
    }

    protected void displayMovieData(MovieData receivedMovieData) {
        movieData = receivedMovieData;

        displayHeader();
        displayChangingData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movie_details, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (movieData != null) {
            if (movieData.getSeenStatus() == MovieData.SeenStatus.NEED_TO_SEE) {
                menu.findItem(R.id.action_addMovieToSee).setVisible(false);
                menu.findItem(R.id.action_addMovieToSee).setTitle(R.string.add_to_watchlist);
                menu.findItem(R.id.action_addSeenMovie).setVisible(true);
                menu.findItem(R.id.action_remove).setVisible(true);
                menu.findItem(R.id.action_rate).setVisible(false);
            } else if (movieData.getSeenStatus() == MovieData.SeenStatus.NOT_ON_LIST) {
                menu.findItem(R.id.action_addMovieToSee).setVisible(true);
                menu.findItem(R.id.action_addMovieToSee).setTitle(R.string.add_to_watchlist);
                menu.findItem(R.id.action_addSeenMovie).setVisible(true);
                menu.findItem(R.id.action_remove).setVisible(false);
                menu.findItem(R.id.action_rate).setVisible(false);
            } else if (movieData.getSeenStatus() == MovieData.SeenStatus.SEEN) {
                menu.findItem(R.id.action_addMovieToSee).setVisible(true);
                menu.findItem(R.id.action_addMovieToSee).setTitle(R.string.add_back_to_watchlist);
                menu.findItem(R.id.action_addSeenMovie).setVisible(false);
                menu.findItem(R.id.action_remove).setVisible(true);
                menu.findItem(R.id.action_rate).setVisible(true);
            }

        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addMovieToSee:
                if (movieData.getSeenStatus() == MovieData.SeenStatus.SEEN) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.are_you_sure)
                            .setMessage(R.string.move_to_watchlist_dialog_message)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    movieData.setSeenStatus(MovieData.SeenStatus.NEED_TO_SEE);
                                    displayChangingData();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                } else {
                    movieData.setSeenStatus(MovieData.SeenStatus.NEED_TO_SEE);
                    movieData.setOnWatchlistSince(Calendar.getInstance());
                    if (MovieDataHolder.getInstance().getMovieById(movieData.id) == null) {
                        movieData = MovieDataHolder.getInstance().addMovie(movieData);
                    }

                    displayChangingData();
                }
                break;
            case R.id.action_addSeenMovie:
                SetMovieSeenStatusDialog dialog = new SetMovieSeenStatusDialog();
                dialog.setListener(new SetMovieSeenStatusDialog.OnMovieDataChangedListener() {
                    @Override
                    public void onMovieDataChanged(MovieData newMovieData) {
                        movieData = newMovieData;
                        displayChangingData();
                    }
                });
                dialog.setMovieData(movieData);
                dialog.show(getSupportFragmentManager(), SetMovieSeenStatusDialog.TAG);

                    /*movieData.setSeenStatus(MovieData.SeenStatus.SEEN_ALL_EPISODES);
                if (MovieDataHolder.getInstance().getMovieById(movieData.id) == null) {
                    MovieDataHolder.getInstance().addMovie(movieData);
                }*/

                break;
            case R.id.action_remove:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.delete_movie_dialog_title)
                        .setMessage(movieData.title + "?")
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                movieData.setSeenStatus(MovieData.SeenStatus.NOT_ON_LIST);
                                movieData.setPersonalRating(0);
                                movieData = MovieDataHolder.getInstance().removeMovie(movieData);
                                displayChangingData();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();

                break;
            case R.id.action_rate:
                RateDialog rateDialog = new RateDialog();
                rateDialog.setListener(new RateDialog.OnRatedListener() {
                    @Override
                    public void onRated() {
                        displayChangingData();
                    }
                });
                rateDialog.setRateable(movieData);
                rateDialog.show(getSupportFragmentManager(), RateDialog.TAG);
                break;

            default:
                break;
        }
        return true;
    }

    @Override
    public MovieData getMovieData() {
        return movieData;
    }
}
