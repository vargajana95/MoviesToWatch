package vargajana95.moviestowatch;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Varga János on 2018. 02. 23..
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
