package vargajana95.moviestowatch.newitem.adapter;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import vargajana95.moviestowatch.model.ResultData;
import vargajana95.moviestowatch.model.SearchResult;
import vargajana95.moviestowatch.network.NetworkManager;

/**
 * Created by Varga János on 2018. 05. 05..
 */

public class SearchAutoCompleteAdapter extends ArrayAdapter<String> {

    private static final int MAX_RESULTS = 6;
    private Context mContext;
    private List<String> suggestions;

    public SearchAutoCompleteAdapter(Context context) {
        super(context, android.R.layout.simple_dropdown_item_1line);
        mContext = context;
        suggestions = new ArrayList<>();
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                suggestions.clear();
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    SearchResult result = null;
                    try {
                        result = NetworkManager.getInstance().searchItems(constraint.toString()).execute().body();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    /*for (ResultData resultData : result.results) {
                        suggestions.add(resultData.getTitle());
                    }*/

                    /*Log.v("LOL", result.results.size()+"");
                    // Assign the data to the FilterResults
                    for (ResultData res : result.results.subList(0, MAX_RESULTS)) {
                        Log.v("LOL", res.getTitle());
                    }*/
                    int j = 0;
                    for (int i = 0; i <result.results.size() && j < MAX_RESULTS; i++) {
                        if (result.results.get(i).media_type.equals("tv") || result.results.get(i).media_type.equals("movie")) {
                            suggestions.add(result.results.get(i).getTitle());
                            j++;
                        }
                    }
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    clear();
                    addAll((List<String>) results.values);
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }

}
