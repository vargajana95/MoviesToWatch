package vargajana95.moviestowatch.newitem.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.model.ResultData;
import vargajana95.moviestowatch.model.Season;
import vargajana95.moviestowatch.model.TvData;
import vargajana95.moviestowatch.movie.MovieDetailsActivity;
import vargajana95.moviestowatch.network.NetworkManager;
import vargajana95.moviestowatch.tv.TvDetailsActivity;

/**
 * Created by Varga János on 2018. 02. 22..
 */

public class NewItemAdapter extends RecyclerView.Adapter<NewItemAdapter.ViewHolder> {

    private List<ResultData> items;
    private Context context;

    public NewItemAdapter(Context context) {
        this.context = context;
        items = new ArrayList<>();//MovieDataHolder.getInstance().getMovies();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final NewItemAdapter.ViewHolder holder, final int position) {
        holder.item = items.get(position);

        holder.tvTitle.setText(holder.item.getTitle());

        holder.tvMediaType.setText(holder.item.getType());
        holder.tvReleaseDate.setText(context.getString(R.string.string_release_date, holder.item.getReleaseDate()));
        holder.tvScore.setText(context.getString(R.string.string_rating, holder.item.vote_average));

        if (holder.item.isLoading()) {
            holder.progressBar.setVisibility(View.VISIBLE);
        }
        else {
            holder.progressBar.setVisibility(View.INVISIBLE);
        }

        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500" +
                        holder.item.poster_path)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_movie))
                .into(holder.ivPoster);

        holder.tvTitle.setSelected(true);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.item.setLoading(true);
                holder.progressBar.setVisibility(View.VISIBLE);
                if (holder.item.media_type.equals("tv")) {
                    //Check in the cache
                    TvData tv = MovieDataHolder.getInstance().getTvById(holder.item.id);
                    if (tv == null) {
                        tv = MovieDataHolder.getInstance().getFromTvCacheById(holder.item.id);
                        if (tv == null)
                            loadTvData(holder);
                        else {
                            Intent intent = new Intent(context, TvDetailsActivity.class);
                            intent.putExtra(TvDetailsActivity.TV_ID, holder.item.id);
                            context.startActivity(intent);
                            holder.item.setLoading(false);
                            holder.progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                    else {
                        Intent intent = new Intent(context, TvDetailsActivity.class);
                        intent.putExtra(TvDetailsActivity.TV_ID, holder.item.id);
                        context.startActivity(intent);
                        holder.item.setLoading(false);
                        holder.progressBar.setVisibility(View.INVISIBLE);
                    }

                }
                else if (holder.item.media_type.equals("movie")) {
                    MovieData movie = MovieDataHolder.getInstance().getMovieById(holder.item.id);
                    if (movie == null) {
                        movie = MovieDataHolder.getInstance().getFromMovieCacheById(holder.item.id);
                        if (movie == null) {
                            loadAndDisplayMovieData(holder);
                        } else {
                            Intent intent = new Intent(context, MovieDetailsActivity.class);
                            intent.putExtra(MovieDetailsActivity.MOVIE_ID, movie.id);
                            context.startActivity(intent);
                            holder.item.setLoading(false);
                            holder.progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                    else {
                        Intent intent = new Intent(context, MovieDetailsActivity.class);
                        intent.putExtra(MovieDetailsActivity.MOVIE_ID, movie.id);
                        context.startActivity(intent);
                        holder.item.setLoading(false);
                        holder.progressBar.setVisibility(View.INVISIBLE);
                    }
                }
                /*Intent intent = new Intent(context, NewMovieDetailsActivity.class);
                intent.putExtra("MOVIE_ID", holder.item.id);
                context.startActivity(intent);*/
            }
        });

    }

    private void loadAndDisplayMovieData(final ViewHolder holder) {
        NetworkManager.getInstance().getMovie(holder.item.id).enqueue(new Callback<MovieData>() {
            @Override
            public void onResponse(Call<MovieData> call,
                                   Response<MovieData> response) {

                Log.d("RETROFIT", "onResponse: " + response.code());
                Log.v("URL", call.request().url().toString());
                if (response.isSuccessful()) {
                    MovieData movie = response.body();
                    MovieDataHolder.getInstance().addMovieToCache(movie);

                    Intent intent = new Intent(context, MovieDetailsActivity.class);
                    intent.putExtra(MovieDetailsActivity.MOVIE_ID, movie.id);
                    context.startActivity(intent);
                    holder.item.setLoading(false);
                    holder.progressBar.setVisibility(View.INVISIBLE);
                } else {
                    Toast.makeText(context,
                            "Error: "+response.message(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MovieData> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(context,
                        R.string.error_in_request,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadTvData(final ViewHolder holder) {
        NetworkManager.getInstance().getTv(holder.item.id).enqueue(new Callback<TvData>() {
            @Override
            public void onResponse(Call<TvData> call,
                                   Response<TvData> response) {

                Log.d("RETROFIT", "onResponse: " + response.code());
                Log.v("URL", call.request().url().toString());
                if (response.isSuccessful()) {
                    TvData tv = response.body();
                    //loadSeason(tv, 0);
                    loadSeasons(tv, holder);
                    /*for (int i = 0; i < tv.number_of_seasons; i++) {
                        loadSeason(tv, i);
                    }*/
                } else {
                    Toast.makeText(context,
                            "Error: "+response.message(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TvData> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(context,
                        R.string.error_in_request,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadSeasons(final TvData tv, final ViewHolder holder) {
        //new LoadSeasonsTask(tv, holder).execute();
        List<Observable<Season>> seasonsObservable = new ArrayList<>();
        for (int i = 0; i < tv.number_of_seasons; i++) {
            seasonsObservable.add(NetworkManager.getInstance().getTvSeason(tv.id, i+1));
        }
        
        Observable.zip(seasonsObservable, new Function<Object[], List<Season>>() {
            @Override
            public List<Season> apply(Object[] objects) throws Exception {
                List<Season> seasons = new ArrayList<>();
                for (Object object : objects) {
                    seasons.add((Season) object);
                }
                return seasons;
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<List<Season>>() {
            private List<Season> seasons;
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Season> seasons) {
                this.seasons = seasons;
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(context, R.string.error_loading_data,Toast.LENGTH_SHORT).show();
                holder.item.setLoading(false);
                holder.progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onComplete() {
                for (int i = 0; i < tv.number_of_seasons; i++) {
                    tv.seasons.set(i, seasons.get(i));
                }
                MovieDataHolder.getInstance().addTvToCache(tv);

                Intent intent = new Intent(context, TvDetailsActivity.class);
                intent.putExtra(TvDetailsActivity.TV_ID, tv.id);
                context.startActivity(intent);
                holder.item.setLoading(false);
                holder.progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }



    public void deleteItems() {
        items.clear();
        notifyDataSetChanged();
    }


    public void addItem(ResultData resultData) {
        items.add(resultData);
        notifyDataSetChanged();
    }

    public void setItems(List<ResultData> items) {
        this.items = items;
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ImageView ivPoster;
        public TextView tvTitle;
        public TextView tvReleaseDate;
        public TextView tvMediaType;
        public TextView tvScore;
        public ProgressBar progressBar;

        public ResultData item;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivPoster = view.findViewById(R.id.ivPoster);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvReleaseDate = view.findViewById(R.id.tvReleaseDate);
            tvMediaType = view.findViewById(R.id.tvMediaType);
            tvScore = view.findViewById(R.id.tvScore);
            progressBar = view.findViewById(R.id.progressBar);

        }
    }
}
