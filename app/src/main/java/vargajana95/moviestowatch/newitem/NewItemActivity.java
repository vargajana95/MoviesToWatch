package vargajana95.moviestowatch.newitem;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vargajana95.moviestowatch.EmptyRecyclerView;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.ResultData;
import vargajana95.moviestowatch.model.SearchResult;
import vargajana95.moviestowatch.network.NetworkManager;
import vargajana95.moviestowatch.newitem.adapter.NewItemAdapter;
import vargajana95.moviestowatch.newitem.adapter.SearchAutoCompleteAdapter;

public class NewItemActivity extends AppCompatActivity {

    private View rootView;
    private NewItemAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_item);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        final EmptyRecyclerView recyclerView = findViewById(R.id.new_item_recycler_view);
        assert recyclerView != null;

        rootView = findViewById(R.id.rootView);


        adapter = new NewItemAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        final DelayAutoCompleteTextView etSearchInput = findViewById(R.id.etSearchInput);
        final ImageButton btnSearch = findViewById(R.id.btnSearch);
        final LinearLayout emptyListLayout = findViewById(R.id.emptyListLayout);

        etSearchInput.setThreshold(3);
        etSearchInput.setAdapter(new SearchAutoCompleteAdapter(this)); // 'this' is Activity instance


        recyclerView.setEmptyView(emptyListLayout);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearchInput.getText().toString().isEmpty()) {
                    etSearchInput.setError(getString(R.string.enter_title));
                    return;
                }

                adapter.deleteItems();
                NetworkManager.getInstance().searchItems(etSearchInput.getText().toString()).enqueue(new Callback<SearchResult>() {
                    @Override
                    public void onResponse(Call<SearchResult> call,
                                           Response<SearchResult> response) {

                        Log.d("RETROFIT", "onResponse: " + response.code());
                        Log.v("URL", call.request().url().toString());
                        if (response.isSuccessful()) {
                            //displayMovieData(response.body());
                            if (response.body().results.isEmpty()) {
                                Snackbar.make(rootView, R.string.no_results,  Snackbar.LENGTH_SHORT).show();
                                return;
                            }

                            List<ResultData> results = new ArrayList<>();
                            for (ResultData result : response.body().results) {
                                if (result.media_type.equals("tv") || result.media_type.equals("movie")) {
                                    results.add(result);
                                }
                            }
                            adapter.setItems(results);

                        } else {
                            Toast.makeText(NewItemActivity.this,
                                    "Error: "+response.message(),
                                    Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<SearchResult> call, Throwable t) {
                        t.printStackTrace();
                        Toast.makeText(NewItemActivity.this,
                                R.string.error_in_request,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
