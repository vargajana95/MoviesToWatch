package vargajana95.moviestowatch.mainscreen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vargajana95.moviestowatch.EmptyRecyclerView;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.mainscreen.adapter.SeenFragmentPagerAdapter;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.movie.adapter.MovieRecyclerViewAdapter;


/**
 * Created by Varga János on 2018. 02. 23..
 */

public class FragmentMovies extends Fragment implements SeenFragmentPagerAdapter.UpdatableFragment{

    private MovieRecyclerViewAdapter adapter;
    private BottomNavigationHolder bottomNavigationHolder;




    public FragmentMovies() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof BottomNavigationHolder) {
            bottomNavigationHolder = (BottomNavigationHolder) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement BottomNavigationHolder interface!");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_movies, container, false);

        EmptyRecyclerView recyclerView = view.findViewById(R.id.movie_recycler_view);
        assert recyclerView != null;


        adapter = new MovieRecyclerViewAdapter(getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(adapter);

        View emptyTV= view.findViewById(R.id.emptyTV);
        recyclerView.setEmptyView(emptyTV);

        TextView tvEmptyText = view.findViewById(R.id.tvEmptyText);
        tvEmptyText.setText(R.string.no_movies);

        return view;
    }


    protected MovieRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }


    @Override
    public void update() {
        int page = bottomNavigationHolder.getBottomNavigationPage();
        switchPage(page);
    }

    private void switchPage(int page) {
        List<MovieData> list = null;
        if (page == MainActivity.BottomNavigationPage.WATCHLIST) {
            list = MovieDataHolder.getInstance().getMoviesToSee();
            Collections.sort(list, new Comparator<MovieData>() {
                @Override
                public int compare(MovieData o1, MovieData o2) {
                    if (o1.getOnWatchlistSinceLong() > o2.getOnWatchlistSinceLong()) return -1;
                    else if (o1.getOnWatchlistSinceLong() < o2.getOnWatchlistSinceLong()) return 1;
                    else return 0;
                }
            });
        }
        else if (page == MainActivity.BottomNavigationPage.SEEN) {
            list = MovieDataHolder.getInstance().getMoviesSeen();
            Collections.sort(list, new Comparator<MovieData>() {
                @Override
                public int compare(MovieData o1, MovieData o2) {
                    if (o1.getSeenDateLong() > o2.getSeenDateLong()) return -1;
                    else if (o1.getSeenDateLong() < o2.getSeenDateLong()) return 1;
                    else return 0;
                }
            });
        }
        else if (page == MainActivity.BottomNavigationPage.ALL) {
            list = MovieDataHolder.getInstance().getMovies();
        }

        adapter.setList(list);
    }
}
