package vargajana95.moviestowatch.mainscreen.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.mainscreen.FragmentMovies;
import vargajana95.moviestowatch.mainscreen.FragmentTvs;

/**
 * Created by Varga János on 2018. 04. 07..
 */

public class SeenFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public SeenFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FragmentMovies();
        } else if (position == 1){
            return new FragmentTvs();
        }

        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        UpdatableFragment f = (UpdatableFragment) object;
        if (f != null) {
            f.update();
        }
        return super.getItemPosition(object);
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.movies);
            case 1:
                return mContext.getString(R.string.tvshows);
            default:
                return null;
        }
    }

    public interface UpdatableFragment {
        void update();
    }

}
