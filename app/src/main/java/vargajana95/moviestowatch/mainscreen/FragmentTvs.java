package vargajana95.moviestowatch.mainscreen;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import vargajana95.moviestowatch.EmptyRecyclerView;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.mainscreen.adapter.SeenFragmentPagerAdapter;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.tv.adapter.TvRecyclerViewAdapter;

/**
 * Created by Varga János on 2018. 02. 24..
 */

public class FragmentTvs extends Fragment implements SeenFragmentPagerAdapter.UpdatableFragment{

    private TvRecyclerViewAdapter adapter;
    private BottomNavigationHolder bottomNavigationHolder;


    public FragmentTvs() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof BottomNavigationHolder) {
            bottomNavigationHolder = (BottomNavigationHolder) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement BottomNavigationHolder interface!");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_movies, container, false);

        EmptyRecyclerView recyclerView = view.findViewById(R.id.movie_recycler_view);
        assert recyclerView != null;


        adapter = new TvRecyclerViewAdapter(getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(adapter);

        View emptyTV= view.findViewById(R.id.emptyTV);
        recyclerView.setEmptyView(emptyTV);

        TextView tvEmptyText = view.findViewById(R.id.tvEmptyText);
        tvEmptyText.setText(R.string.no_tvshows);

        update();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    protected TvRecyclerViewAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void update() {
        int page = bottomNavigationHolder.getBottomNavigationPage();
        switchPage(page);
    }

    private void switchPage(int page) {
        if (page == MainActivity.BottomNavigationPage.WATCHLIST)
            adapter.setList(MovieDataHolder.getInstance().getTvsToSee());
        else if (page == MainActivity.BottomNavigationPage.SEEN)
            adapter.setList(MovieDataHolder.getInstance().getTvsSeen());
        else if (page == MainActivity.BottomNavigationPage.ALL)
            adapter.setList(MovieDataHolder.getInstance().getTvs());
    }
}
