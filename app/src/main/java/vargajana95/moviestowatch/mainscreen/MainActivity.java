package vargajana95.moviestowatch.mainscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.mainscreen.adapter.SeenFragmentPagerAdapter;
import vargajana95.moviestowatch.newitem.NewItemActivity;


public class MainActivity extends AppCompatActivity implements BottomNavigationHolder {
    public interface BottomNavigationPage {
        int WATCHLIST = 0;
        int SEEN = 1;
        int ALL = 2;
    }

    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private boolean moviesTabSelected = true;

    private int bottomNavigationPage;

    private TabLayout tabLayout;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_watchlist:
                    bottomNavigationPage = BottomNavigationPage.WATCHLIST;
                    viewPager.getAdapter().notifyDataSetChanged();
                    setTitle(getString(R.string.watchlist));
                    return true;
                case R.id.navigation_seen:
                    bottomNavigationPage = BottomNavigationPage.SEEN;
                    viewPager.getAdapter().notifyDataSetChanged();
                    setTitle(getString(R.string.seen));
                    return true;
                case R.id.navigation_all:
                    bottomNavigationPage = BottomNavigationPage.ALL;
                    viewPager.getAdapter().notifyDataSetChanged();
                    setTitle(getString(R.string.all));
                    return true;
            }
            return false;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = findViewById(R.id.tabLayout);
        //tabLayout.addOnTabSelectedListener(tabSelectedListener);

        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, fab,"newitem");
                Intent i = new Intent();
                i.setClass(MainActivity.this, NewItemActivity.class);
                startActivity(i, options.toBundle());
            }
        });

        viewPager = findViewById(R.id.viewpager);

        viewPager.setAdapter(new SeenFragmentPagerAdapter(this, getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        /*int[] ids = {284053, 11, 198663, 335984, 64956, 263115};

        for (int id : ids) {
            loadMovie(id);
        }*/

        //changeFragment(FragmentMoviesNeedToSee.class);

        setTitle(getString(R.string.watchlist));

    }



    @Override
    public int getBottomNavigationPage() {
        return bottomNavigationPage;
    }
}
