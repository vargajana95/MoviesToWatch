package vargajana95.moviestowatch.mainscreen;


/**
 * Created by Varga János on 2018. 04. 07..
 */

public interface BottomNavigationHolder {
    int getBottomNavigationPage();
}
