package vargajana95.moviestowatch.model;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import vargajana95.moviestowatch.PostProcessingEnabler;

/**
 * Created by Varga János on 2018. 02. 23..
 */

public class TvData extends RealmObject implements PostProcessingEnabler.PostProcessable, Rateable {
    public static String baseImdbLink = "www.imdb.com/title/";
    public static String baseTmdbLink = "www.themoviedb.org/tv/";

    private static final String RETURNING_SERIES = "Returning Series";
    private static final String ENDED = "Ended";

    public String backdrop_path ="";
    public RealmList<Integer> episode_run_time = null;
    public String first_air_date ="";
    public RealmList<Genre> genres = null;
    public String homepage = "";
    public Integer id;
    public Boolean in_production = false;
    public String last_air_date ="";
    public String name = "";
    public Integer number_of_episodes = 0;
    public Integer number_of_seasons = 0;
    public String original_name ="";
    public String overview ="";
    public Double popularity;
    public String poster_path ="";
    public RealmList<Season> seasons = null;
    public String status ="";
    public String type ="";
    public Double vote_average = 0.0;
    public Integer vote_count = 0;

    public Credits credits;

    public RealmList<Crew> created_by;

    private double personalRating;


    private int seenStatus;

    public ExternalIds external_ids;


    public String getTopActorsAsString(int top) {
        if (credits.cast.size() == 0)
            return "";

        StringBuilder castString = new StringBuilder(credits.cast.get(0).name);

        for (int i = 1; i < credits.cast.size() && i < top; i++) {
            castString.append(", ").append(credits.cast.get(i).name);
        }

        return castString.toString();
    }

    public String getCreatorsAsString(int max) {
        if (created_by.size() == 0)
            return "";

        StringBuilder creatorsString = new StringBuilder(created_by.get(0).name);

        for (int i = 1; i < created_by.size() && i < max; i++) {
            creatorsString.append(", ").append(created_by.get(i).name);
        }

        return creatorsString.toString();
    }

    public String getGenresAsString() {
        if (genres.size() == 0)
            return "";

        StringBuilder genresString = new StringBuilder(genres.get(0).name);

        for (int i = 1; i < genres.size(); i++) {
            genresString.append(", ").append(genres.get(i).name);
        }

        return genresString.toString();
    }

    public void setSeenStatus(int seenStatus) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.seenStatus = seenStatus;
        realm.commitTransaction();
    }

    public double getPersonalRating() {
        return personalRating;
    }

    @Override
    public void setPersonalRating(double personalRating) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.personalRating = personalRating;
        realm.commitTransaction();
    }

    public void setAllEpisodesSeen(boolean b) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        for (int i = 0; i < number_of_seasons; i++) {
            Season season = seasons.get(i);
            for (Episode episode : season.episodes) {
                episode.setSeenWithNoTransaction(b);
                episode.setSeenDateWithNoTransaction(null);
            }
        }
        realm.commitTransaction();

    }

    public void setSeenSeasons(List<Season> seenSeasons) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        for (int i = 0; i < seenSeasons.size(); i++) {
            Season season = seenSeasons.get(i);
            for (Episode episode : season.episodes) {
                episode.setSeenWithNoTransaction(true);
                //episode.setSeenDateWithNoTransaction(null);
            }
        }
        realm.commitTransaction();
    }

    //Called when object is loaded
    @Override
    public void gsonPostProcess() {
        //Removing the "Specials" from the seasons
        if (seasons != null && !seasons.isEmpty()) {
            if (seasons.get(0).season_number == 0)
                seasons.remove(0);
        }
    }

    public void calculateSeenStatus() {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        for (int i = 0; i < number_of_seasons; i++) {
            Season season = seasons.get(i);
            for (Episode episode : season.episodes) {
                if (!episode.isSeen()) {
                    seenStatus = SeenStatus.WATCHLIST;
                    realm.commitTransaction();
                    return;
                }
            }
        }
        seenStatus = SeenStatus.SEEN_ALL_EPISODES;
        realm.commitTransaction();
    }


    public interface SeenStatus {
        int NOT_ON_LIST = 0;
        int WATCHLIST = 1;
        int SEEN_ALL_EPISODES = 2;
    }

    public int getSeenStatus() {
        return seenStatus;
    }

    public int getSeenPercent() {
        int numberOfEpisodesSeen = 0;
        int numberOfEpisodes = 0;
        for (int i = 0; i < number_of_seasons; i++) {
            for (Episode episode : seasons.get(i).episodes) {
                numberOfEpisodes++;
                if (episode.isSeen())
                    numberOfEpisodesSeen++;
            }
        }

        if (numberOfEpisodes == 0)
            return 0;
        else
            return (int) Math.floor((double) numberOfEpisodesSeen / numberOfEpisodes * 100);
    }

    public Episode getLastSeenEpisode() {
        Episode episodeLastSeen = new Episode();
        for (int i = 0; i < number_of_seasons; i++) {
            for (Episode episode : seasons.get(i).episodes) {
                if (episode.isSeen() && episode.getSeenDateInMillis() >= episodeLastSeen.getSeenDateInMillis())
                    episodeLastSeen = episode;
            }
        }

        if (!episodeLastSeen.isSeen())
            return null;

        return episodeLastSeen;
    }

    public String getAirDate() {
        if (first_air_date == null)
            return "";

        if (status.equals(RETURNING_SERIES)) {
            return first_air_date.split("-")[0] + " - present";
        } else {
            return first_air_date.split("-")[0] + " - " + last_air_date.split("-")[0];
        }
    }

    public String getImdbLink() {
        return baseImdbLink + external_ids.imdb_id;
    }

    public String getTmdbLink() {
        return baseTmdbLink + id;
    }


}
