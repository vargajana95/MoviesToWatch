package vargajana95.moviestowatch.model;

/**
 * Created by Varga János on 2018. 02. 24..
 */

public class ResultData {
    public int id;
    //For the tv shows
    private String name ="";
    //For the movies
    private String title ="";
    public double vote_average;
    public String poster_path ="";
    public String media_type ="";
    private String release_date ="";
    private String first_air_date ="";

    private boolean loading;

    public String getTitle() {
        return media_type.equals("tv") ? name : title;
    }

    public String getReleaseDate() {
        return media_type.equals("tv") ? first_air_date.split("-")[0] : release_date.split("-")[0];
    }

    public String getType() {
        return media_type.equals("tv") ? "Tv show" : "Movie";
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
