package vargajana95.moviestowatch.model;

import io.realm.RealmObject;

/**
 * Created by Varga János on 2018. 02. 21..
 */

public class Cast extends RealmObject {

    public Integer castId;
    public String character;
    public String creditId;
    public Integer gender;
    public Integer id;
    public String name;
    public Integer order;
    public String profilePath;

}
