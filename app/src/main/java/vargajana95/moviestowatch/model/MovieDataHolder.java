package vargajana95.moviestowatch.model;


import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by Varga János on 2018. 02. 22..
 */

public class MovieDataHolder {
    private ArrayList<MovieData> movies;
    private ArrayList<TvData> tvs;
    private List<TvData> tvCache;
    private List<MovieData> movieCache;

    private static MovieDataHolder instance = null;

    private MovieDataHolder() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<MovieData> queryMovies = realm.where(MovieData.class);
        RealmQuery<TvData> queryTvs = realm.where(TvData.class);

        movies = new ArrayList<>();
        tvs = new ArrayList<>();
        movies.addAll(queryMovies.findAll());
        tvs.addAll(queryTvs.findAll());

        tvCache = new ArrayList<>();
        movieCache = new ArrayList<>();
    }

    public static MovieDataHolder getInstance() {
        if (instance == null)
            instance = new MovieDataHolder();

        return instance;
    }

    /*public ArrayList<MovieData> getMovies() {
        return movies;
    }*/

    public MovieData getMovie(int index) {
        return movies.get(index);
    }

    public MovieData addMovie(MovieData movie) {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        MovieData newMovieData = realm.copyToRealm(movie);
        movies.add(newMovieData);
        realm.commitTransaction();
        return newMovieData;
    }

    public TvData addTv(TvData tv) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        TvData newTvData = realm.copyToRealm(tv);
        tvs.add(newTvData);
        realm.commitTransaction();
        return newTvData;
    }

    public MovieData getMovieById(int movieId) {
        for (MovieData movie : movies) {
            if (movie.id == movieId)
                return movie;
        }
        //TODO return with nullObject
        return  null;
    }

    public TvData getTvById(int tvId) {
        for (TvData tv : tvs) {
            if (tv.id == tvId)
                return tv;
        }
        //TODO return with nullObject
        return  null;
    }

    public List<MovieData> getMoviesSeen() {
        ArrayList<MovieData> moviesSeen = new ArrayList<>();

        for (MovieData movie : movies) {
            if (movie.getSeenStatus() == MovieData.SeenStatus.SEEN) {
                moviesSeen.add(movie);
            }
        }
        return moviesSeen;
    }

    public List<MovieData> getMoviesToSee() {
        ArrayList<MovieData> moviesToSee = new ArrayList<>();

        for (MovieData movie : movies) {
            if (movie.getSeenStatus() == MovieData.SeenStatus.NEED_TO_SEE) {
                moviesToSee.add(movie);
            }
        }
        return moviesToSee;
    }

    public List<TvData> getTvs() {
        return (ArrayList<TvData>) tvs.clone();
    }

    public List<MovieData> getMovies() {
        return (ArrayList<MovieData>) movies.clone();
    }

    public List<TvData> getTvsSeen() {
        ArrayList<TvData> tvsSeen = new ArrayList<>();

        for (TvData tv : tvs) {
            if (tv.getSeenStatus() == MovieData.SeenStatus.SEEN) {
                tvsSeen.add(tv);
            }
        }
        return tvsSeen;
    }

    public List<TvData> getTvsToSee() {
        ArrayList<TvData> tvsSeen = new ArrayList<>();

        for (TvData tv : tvs) {
            if (tv.getSeenStatus() == MovieData.SeenStatus.NEED_TO_SEE) {
                tvsSeen.add(tv);
            }
        }
        return tvsSeen;
    }

    public void addTvToCache(TvData tvData) {
        //TODO: make circular list
        tvCache.add(tvData);
    }

    public TvData getFromTvCacheById(int tvId) {
        for (TvData tv : tvCache) {
            if (tv.id == tvId)
                return tv;
        }
        //TODO return with nullObject
        return  null;
    }


    public void addMovieToCache(MovieData movieData) {
        //TODO: make circular list
        movieCache.add(movieData);
    }

    public MovieData getFromMovieCacheById(int movieId) {
        for (MovieData movie : movieCache) {
            if (movie.id == movieId)
                return movie;
        }
        //TODO return with nullObject
        return  null;
    }

    public MovieData removeMovie(MovieData movieData) {
        MovieData unmanagedMovieData;
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        movies.remove(movieData);
        unmanagedMovieData = realm.copyFromRealm(movieData);
        realm.where(MovieData.class).equalTo("id",movieData.id).findAll().deleteAllFromRealm();
        realm.commitTransaction();

        return unmanagedMovieData;
    }

    public TvData removeTv(TvData tvData) {
        TvData unmanagedTvData;
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        tvs.remove(tvData);
        unmanagedTvData = realm.copyFromRealm(tvData);
        realm.where(MovieData.class).equalTo("id",tvData.id).findAll().deleteAllFromRealm();
        realm.commitTransaction();

        return unmanagedTvData;
    }
}
