package vargajana95.moviestowatch.model;

import io.realm.RealmObject;

/**
 * Created by Varga János on 2018. 02. 21..
 */

public class Crew extends RealmObject {

    public String creditId;
    public String department;
    public Integer gender;
    public Integer id;
    public String job;
    public String name;
}
