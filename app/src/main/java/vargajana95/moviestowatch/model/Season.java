package vargajana95.moviestowatch.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Varga János on 2018. 02. 23..
 */

public class Season extends RealmObject {
    public String airDate;
    public RealmList<Episode> episodes = null;
    public String name;
    public String overview;
    public Integer id;
    public String poster_path;
    public Integer season_number;
    public Integer episode_count;

    public boolean isAllEpisodesSeen() {
        for (Episode episode : episodes) {
            if (!episode.isSeen())
                return false;
        }
        return true;
    }
}
