package vargajana95.moviestowatch.model;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Varga János on 2018. 02. 23..
 */

public class Episode extends RealmObject implements Rateable {
    public String air_date ="";
    public RealmList<Crew> crew = null;
    public Integer episode_number;
    public String name ="";
    public String overview ="";
    public Integer id;
    public Integer season_number;
    public String still_path;
    public Double vote_average;
    public Integer vote_count;

    private long seenDate;


    private boolean seen;

    private double personalRating;

    public double getPersonalRating() {
        return personalRating;
    }

    public void setPersonalRating(double personalRating) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.personalRating = personalRating;
        realm.commitTransaction();
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        this.seen = seen;
        realm.commitTransaction();
    }

    public void setSeenWithNoTransaction(boolean seen) {
        this.seen = seen;
    }

    public void setSeenDate(Calendar seenDate) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        setSeenDateWithNoTransaction(seenDate);
        realm.commitTransaction();
    }

    public void setSeenDateWithNoTransaction(Calendar seenDate) {
        if (seenDate == null)
            this.seenDate = 0;
        else
            this.seenDate = seenDate.getTimeInMillis();
    }

    public Calendar getSeenDate() {
        Calendar calendar =Calendar.getInstance();
        if (seenDate == 0)
            return null;
        else {
            calendar.setTimeInMillis(seenDate);
            return calendar;
        }
    }

    public long getSeenDateInMillis() {
        return seenDate;
    }

}
