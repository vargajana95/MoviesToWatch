package vargajana95.moviestowatch.model;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Varga János on 2018. 02. 21..
 */

public class MovieData extends RealmObject implements Rateable{
    public static String baseImdbLink = "www.imdb.com/title/";
    public static String baseTmdbLink = "www.themoviedb.org/movie/";

    public Boolean adult;
    public String backdrop_path;
    public RealmList<Genre> genres = null;
    public String homepage;
    public Integer id;
    public String imdb_id;
    public String original_title;
    public String overview;
    public String poster_path;
    public String release_date;
    public Integer revenue;
    public Integer runtime;
    public String status;
    public String tagline;
    public String title;
    public Double vote_average;
    public Integer vote_count;
    public Credits credits;

    private int seenStatus;
    private long seenDate;
    private long onWatchlistSince;
    private double personalRating;

    public double getPersonalRating() {
        return personalRating;
    }

    public void setPersonalRating(double personalRating) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.personalRating = personalRating;
        realm.commitTransaction();
    }


    public interface SeenStatus {
        int NOT_ON_LIST = 0;
        int NEED_TO_SEE = 1;
        int SEEN = 2;
    }

    public void setSeenDate(Calendar seenDate) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.seenDate = seenDate.getTimeInMillis();
        realm.commitTransaction();
    }

    public Calendar getSeenDate() {
        Calendar calendar =Calendar.getInstance();
        calendar.setTimeInMillis(seenDate);

        return calendar;
    }

    public void setOnWatchlistSince(Calendar watchlistSince) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.onWatchlistSince = watchlistSince.getTimeInMillis();
        realm.commitTransaction();
    }

    public Calendar getOnWatchlistSince() {
        Calendar calendar =Calendar.getInstance();
        calendar.setTimeInMillis(onWatchlistSince);

        return calendar;
    }


    public int getSeenStatus() {
        return seenStatus;
    }

    public void setSeenStatus(int seenStatus) {
        Realm realm = Realm.getDefaultInstance();

        realm.beginTransaction();
        this.seenStatus = seenStatus;
        realm.commitTransaction();
    }

    public String getGenresAsString() {
        if (genres.size() == 0)
            return "";

        StringBuilder genresString = new StringBuilder(genres.get(0).name);

        for (int i = 1; i < genres.size(); i++) {
            genresString.append(", ").append(genres.get(i).name);
        }

        return genresString.toString();
    }

    public String getTopActorsAsString(int top) {
        if (credits.cast.size() == 0)
            return "";

        StringBuilder castString = new StringBuilder(credits.cast.get(0).name);

        for (int i = 1; i < credits.cast.size() && i < top; i++) {
            castString.append(", ").append(credits.cast.get(i).name);
        }

        return castString.toString();
    }

    public String getDirectorsAsString() {
        if (credits.crew.size() == 0)
            return "";

        StringBuilder directorString = new StringBuilder();

        for (int i = 0; i < credits.crew.size(); i++) {
            if (credits.crew.get(i).job.equals("Director")) {
                if (directorString.length() != 0)
                    directorString.append(", ");

                directorString.append(credits.crew.get(i).name);
            }
        }


        return directorString.toString();
    }

    public long getSeenDateLong() {
        return seenDate;
    }

    public long getOnWatchlistSinceLong() {
        return onWatchlistSince;
    }
    public String getImdbLink() {
        return baseImdbLink + imdb_id;
    }

    public String getTmdbLink() {
        return baseTmdbLink + id;
    }
}
