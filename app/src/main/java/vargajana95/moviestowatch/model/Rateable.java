package vargajana95.moviestowatch.model;

/**
 * Created by Varga János on 2018. 04. 06..
 */

public interface Rateable {
    double getPersonalRating();

    void setPersonalRating(double personalRating);
}
