package vargajana95.moviestowatch.model;

import io.realm.RealmObject;

/**
 * Created by Varga János on 2018. 02. 21..
 */

public class Genre extends RealmObject {

    public Integer id;
    public String name;

}
