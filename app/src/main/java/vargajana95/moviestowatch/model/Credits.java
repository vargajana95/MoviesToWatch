package vargajana95.moviestowatch.model;

/**
 * Created by Varga János on 2018. 02. 21..
 */

import io.realm.RealmList;
import io.realm.RealmObject;

public class Credits extends RealmObject {

    public RealmList<Cast> cast = null;
    public RealmList<Crew> crew = null;

}
