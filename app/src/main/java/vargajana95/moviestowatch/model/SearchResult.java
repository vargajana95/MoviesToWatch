package vargajana95.moviestowatch.model;

import java.util.List;

/**
 * Created by Varga János on 2018. 02. 22..
 */

public class SearchResult {
        public Integer page;
        public Integer totalResults;
        public Integer totalPages;
        public List<ResultData> results = null;
}
