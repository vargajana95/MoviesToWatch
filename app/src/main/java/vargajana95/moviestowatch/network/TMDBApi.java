package vargajana95.moviestowatch.network;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.SearchResult;
import vargajana95.moviestowatch.model.Season;
import vargajana95.moviestowatch.model.TvData;

/**
 * Created by Varga János on 2018. 02. 21..
 */

public interface TMDBApi {

        @GET("/3/movie/{id}")
        Call<MovieData> getMovie(@Path("id") Integer movieId, @Query("api_key") String apiKey, @Query("append_to_response") String appendToResponse);

        @GET("/3/search/multi")
        Call<SearchResult> searchItems(@Query("api_key") String apiKey, @Query("query") String query);

        @GET("/3/tv/{id}")
        Call<TvData> getTv(@Path("id") Integer tvId, @Query("api_key") String apiKey, @Query("append_to_response") String appendToResponse);

        @GET("/3/tv/{id}/season/{seasonNum}")
        Observable<Season> getTvSeason(@Path("id") Integer tvId, @Path("seasonNum") Integer seasonNum, @Query("api_key") String apiKey);
}
