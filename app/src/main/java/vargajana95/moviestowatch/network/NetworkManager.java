package vargajana95.moviestowatch.network;

import com.google.gson.GsonBuilder;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import vargajana95.moviestowatch.PostProcessingEnabler;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.SearchResult;
import vargajana95.moviestowatch.model.Season;
import vargajana95.moviestowatch.model.TvData;

public class NetworkManager {

    private static final String ENDPOINT_ADDRESS = "http://api.themoviedb.org";
    private static final String API_KEY = "ca9c8f702bccfa844a3b935fe0b4ee25";

    private static NetworkManager instance;

    public static NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    private Retrofit retrofit;
    private TMDBApi tmdpApi;

    private NetworkManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT_ADDRESS)
                .client(new OkHttpClient.Builder().build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().registerTypeAdapterFactory(new PostProcessingEnabler()).create()))
                .build();
        tmdpApi = retrofit.create(TMDBApi.class);
    }

    public Call<MovieData> getMovie(Integer movieId) {
        return tmdpApi.getMovie(movieId, API_KEY, "credits");
    }
    public Call<SearchResult> searchItems(String query) {
        return tmdpApi.searchItems(API_KEY, query);
    }

    public Call<TvData> getTv(int tvId) {
        return tmdpApi.getTv(tvId, API_KEY, "credits,external_ids");
    }

    public Observable<Season> getTvSeason(int tvId, int seasonNum) {
        return tmdpApi.getTvSeason(tvId, seasonNum, API_KEY);
    }


}