package vargajana95.moviestowatch;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import vargajana95.moviestowatch.model.Rateable;

/**
 * Created by Varga János on 2018. 04. 06..
 */

public class RateDialog extends AppCompatDialogFragment {
    public static final String TAG = "RateDialog";

    private OnRatedListener listener;

    private EditText etPersonalRating;
    private Rateable rateable;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.rate_dialog_title)
                .setView(getContentView())
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel, null)
                .create();


        //Added this to add validation. Don't want to close the dialog on OK immediately
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isValid()) {
                            rateable.setPersonalRating(Double.parseDouble(etPersonalRating.getText().toString()));
                            listener.onRated();
                            dialog.dismiss();

                        }
                    }
                });
            }
        });
        return dialog;
    }


    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_rate, null);

        etPersonalRating = view.findViewById(R.id.etPersonalRating);

        if (rateable != null && rateable.getPersonalRating() != 0)
            etPersonalRating.setText(Double.toString(rateable.getPersonalRating()));

        return view;
    }

    public void setRateable(Rateable rateable) {
        this.rateable = rateable;
    }

    public void setListener(OnRatedListener listener) {
        this.listener = listener;
    }



    private boolean isValid() {
        try {
            double rate = Double.parseDouble(etPersonalRating.getText().toString());
            if (rate < 1 || rate > 10) {
                etPersonalRating.setError(getString(R.string.rate_input_error));
                return false;
            }
        } catch (NumberFormatException e) {
            etPersonalRating.setError(getString(R.string.rate_input_error));
            return false;
        }

        return true;
    }

    public interface OnRatedListener {
        void onRated();
    }
}
