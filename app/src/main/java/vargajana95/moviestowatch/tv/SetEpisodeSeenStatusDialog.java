package vargajana95.moviestowatch.tv;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.Episode;

/**
 * Created by Varga János on 2018. 02. 24..
 */

public class SetEpisodeSeenStatusDialog extends AppCompatDialogFragment {
    public static final String TAG = "SetMovieSeenStatusDialog";

    private OnEpisodeDataChangedListener listener;

    private DatePicker dpDate;
    private EditText etPersonalRating;
    private Episode episode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getActivity() instanceof OnEpisodeDataChangedListener) {
            listener = (OnEpisodeDataChangedListener) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement OnEpisodeDataChangedListener interface!");
        }*/
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.i_have_seen_this)
                .setView(getContentView())
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel, null)
                .create();


        //Added this to add validation. Don't want to close the dialog on OK immediately
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                okButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isValid()) {
                            modifyEpisode();
                            /*if (MovieDataHolder.getInstance().getMovieById(movieData.id) == null) {
                                MovieDataHolder.getInstance().addMovie(movieData);
                            }*/
                            listener.onEpisodeDataChanged();
                            dialog.dismiss();

                        }
                    }
                });
            }
        });
        return dialog;
    }

    private void modifyEpisode() {
        episode.setSeen(true);
        double score;
        try {
            score = Double.parseDouble(etPersonalRating.getText().toString());
        } catch (NumberFormatException e) {
            score = 0;
        }
        episode.setPersonalRating(score);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, dpDate.getYear());
        calendar.set(Calendar.MONTH, dpDate.getMonth());
        calendar.set(Calendar.DAY_OF_MONTH, dpDate.getDayOfMonth());
        episode.setSeenDate(calendar);
    }


    private View getContentView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_seen_status, null);

        dpDate = view.findViewById(R.id.dpDate);
        etPersonalRating = view.findViewById(R.id.etPersonalRating);

        return view;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

    public void setListener(OnEpisodeDataChangedListener listener) {
        this.listener = listener;
    }


    private boolean isValid() {

        return true;
    }

    public interface OnEpisodeDataChangedListener {
        void onEpisodeDataChanged();
    }


}
