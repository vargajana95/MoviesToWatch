package vargajana95.moviestowatch.tv;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import java.util.ArrayList;
import java.util.List;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.model.Season;
import vargajana95.moviestowatch.model.TvData;

/**
 * Created by Varga János on 2018. 05. 02..
 */

public class SelectSeenSeasonsDialog extends AppCompatDialogFragment {
    public static final String TAG = "SelectSeenSeasonsDialog";
    private List<Season> seenSeasons;
    private TvData tvData;
    private OnTvChangedListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        seenSeasons = new ArrayList<>();
        String[] seasonNames = new String[tvData.number_of_seasons];
        for (int i = 0; i < tvData.number_of_seasons; i++) {
            seasonNames[i] = tvData.seasons.get(i).name;
        }

        boolean[] checkedItems = new boolean[tvData.number_of_seasons];
        int i = 0;
        for (Season season : tvData.seasons) {
            checkedItems[i++] = season.isAllEpisodesSeen();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select_seasons_dialog_title)
                .setMultiChoiceItems(seasonNames, checkedItems,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    // If the user checked the item, add it to the selected items
                                    seenSeasons.add(tvData.seasons.get(which));
                                } else {
                                    // Else, if the item is already in the array, remove it
                                    seenSeasons.remove(tvData.seasons.get(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        TvData newTvData;
                        tvData.setSeenSeasons(seenSeasons);
                        if (MovieDataHolder.getInstance().getTvById(tvData.id) == null) {
                            newTvData = MovieDataHolder.getInstance().addTv(tvData);
                        } else newTvData = tvData;
                        listener.onTvChanged(newTvData);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setTvData(TvData tvData) {
        this.tvData = tvData;
    }

    public void setListener(OnTvChangedListener listener) {
        this.listener = listener;
    }

    public interface OnTvChangedListener {
        void onTvChanged(TvData newTvData);
    }
}
