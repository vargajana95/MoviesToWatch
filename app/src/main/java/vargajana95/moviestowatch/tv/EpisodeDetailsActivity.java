package vargajana95.moviestowatch.tv;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import vargajana95.moviestowatch.BaseDetailsActivity;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.RateDialog;
import vargajana95.moviestowatch.model.Episode;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.model.TvData;
import vargajana95.moviestowatch.movie.SetMovieSeenStatusDialog;

public class EpisodeDetailsActivity extends BaseDetailsActivity {

    public static final String TV_ID = "TV_ID";

    public static final String EPISODE_NUMBER = "EPISODE_NUMBER";
    public static final String SEASON_NUMBER = "SEASON_NUMBER";

    private TextView tvGenres;
    private TextView tvOverView;
    private TextView tvSeason;
    private TextView tvEpisode;
    private TextView tvRating;
    private TextView tvPersonalScore;
    private TextView tvWatchDate;
    private TextView tvReleaseDate;
    private TextView tvRuntime;
    private Button btnWatch;
    private LinearLayout dateView;
    private LinearLayout personalScoreLayout;

    private TvData tvData;
    private int currentSeasonIndex;
    private int currentEpisodeIndex;

    private Episode currentEpisode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episode_details);

        bindActivity();

        tvGenres = findViewById(R.id.tvGenres);

        tvOverView = findViewById(R.id.tvOverView);
        tvSeason = findViewById(R.id.tvSeason);
        tvRating = findViewById(R.id.tvRating);
        tvEpisode = findViewById(R.id.tvEpisode);
        tvPersonalScore = findViewById(R.id.tvPersonalScore);
        tvWatchDate = findViewById(R.id.tvWatchDate);
        btnWatch = findViewById(R.id.btnWatch);
        dateView = findViewById(R.id.dateView);
        personalScoreLayout = findViewById(R.id.personalScoreLayout);
        tvReleaseDate = findViewById(R.id.tvReleaseDate);
        tvRuntime = findViewById(R.id.tvRuntime);

        tvGenres.setSelected(true);

        int tvID=getIntent().getExtras().getInt(TV_ID, -1);

        if (tvID != -1) {
            //TvData tvToDisplay = MovieDataHolder.getInstance().getTvById(tvID);
            currentSeasonIndex =getIntent().getExtras().getInt(SEASON_NUMBER, 0);
            currentEpisodeIndex =getIntent().getExtras().getInt(EPISODE_NUMBER, 0);
            //if (tvToDisplay != null)

            TvData tvData = MovieDataHolder.getInstance().getTvById(tvID);
            if (tvData != null) {
                displayTvData(tvData);
            }
            else {
                //We try to get this from the cache
                tvData = MovieDataHolder.getInstance().getFromTvCacheById(tvID);
                if (tvData != null) {
                    displayTvData(tvData);
                } else {
                    //TvData was not in the cache
                    //loadAndDisplayTvData(tvID);
                    finish();
                }
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        displaySeenPanel();
    }

    @Override
    protected void displaySeenPanel() {
        if (currentEpisode.isSeen()) {
            btnWatch.setBackground(getResources().getDrawable(R.drawable.button_seen));
            btnWatch.setText(R.string.seen_it);
            btnWatch.setTextColor(getResources().getColor(R.color.buton_seen));
            if (currentEpisode.getSeenDate() != null) {
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                tvWatchDate.setText("on " + format1.format(currentEpisode.getSeenDate().getTime()));
                dateView.setVisibility(View.VISIBLE);
            }
            else {
                dateView.setVisibility(View.INVISIBLE);
            }
        }
        else {
            btnWatch.setBackground(getResources().getDrawable(R.drawable.button_not_seen));
            btnWatch.setText(R.string.not_seen);
            btnWatch.setTextColor(getResources().getColor(R.color.button_not_seen));
            dateView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void displayInfo() {
        tvOverView.setText(currentEpisode.overview);
        tvSeason.setText(String.format(Locale.getDefault(), "%d", currentSeasonIndex +1));
        tvEpisode.setText((String.format(Locale.getDefault(), "%d", currentEpisodeIndex +1)));
        tvRating.setText(getString(R.string.string_rating, currentEpisode.vote_average));
        if (currentEpisode.getPersonalRating() == 0 || !currentEpisode.isSeen()) {
            personalScoreLayout.setVisibility(View.GONE);
        }
        else {
            personalScoreLayout.setVisibility(View.VISIBLE);
            tvPersonalScore.setText(getString(R.string.string_rating, currentEpisode.getPersonalRating()));
        }
    }

    @Override
    protected void displayHeader() {
        setPosterImage("https://image.tmdb.org/t/p/w500" + tvData.seasons.get(currentSeasonIndex).poster_path);

        setBackdropImage("https://image.tmdb.org/t/p/w500" + currentEpisode.still_path);

        tvGenres.setText(tvData.getGenresAsString());
        tvReleaseDate.setText(currentEpisode.air_date);
        tvRuntime.setText(getString(R.string.run_time, tvData.episode_run_time.get(0)));

        setCollapsingTitle(currentEpisode.name);
    }


    protected void displayTvData(TvData receivedTvData) {
        tvData = receivedTvData;
        currentEpisode = tvData.seasons.get(currentSeasonIndex).episodes.get(currentEpisodeIndex);
        //MovieDataHolder.getInstance().addTvToCache(tvData);

        displayChangingData();
        displayHeader();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_episode_details, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (currentEpisode != null) {
            if (!currentEpisode.isSeen() && tvData.getSeenStatus() == TvData.SeenStatus.NOT_ON_LIST){
                menu.findItem(R.id.action_addTvToWatchlist).setVisible(true);
                menu.findItem(R.id.action_addEpisodeSeen).setVisible(true);
                menu.findItem(R.id.action_addEpisodeToWatchlist).setVisible(false);
                menu.findItem(R.id.action_rate).setVisible(false);
            }
            else if (!currentEpisode.isSeen() && tvData.getSeenStatus() != TvData.SeenStatus.NOT_ON_LIST){
                menu.findItem(R.id.action_addTvToWatchlist).setVisible(false);
                menu.findItem(R.id.action_addEpisodeSeen).setVisible(true);
                menu.findItem(R.id.action_addEpisodeToWatchlist).setVisible(false);
                menu.findItem(R.id.action_rate).setVisible(false);
            }
            else if (currentEpisode.isSeen() && tvData.getSeenStatus() != TvData.SeenStatus.NOT_ON_LIST){
                menu.findItem(R.id.action_addTvToWatchlist).setVisible(false);
                menu.findItem(R.id.action_addEpisodeSeen).setVisible(false);
                menu.findItem(R.id.action_addEpisodeToWatchlist).setVisible(true);
                menu.findItem(R.id.action_rate).setVisible(true);
            }

        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addTvToWatchlist:
                tvData.setSeenStatus(TvData.SeenStatus.WATCHLIST);
                if (MovieDataHolder.getInstance().getTvById(tvData.id) == null) {
                    tvData = MovieDataHolder.getInstance().addTv(tvData);
                    currentEpisode = tvData.seasons.get(currentSeasonIndex).episodes.get(currentEpisodeIndex);
                }
                displayChangingData();
                break;
            case R.id.action_addEpisodeSeen:
                SetEpisodeSeenStatusDialog dialog = new SetEpisodeSeenStatusDialog();
                dialog.setEpisode(currentEpisode);
                dialog.setListener(new SetEpisodeSeenStatusDialog.OnEpisodeDataChangedListener() {
                    @Override
                    public void onEpisodeDataChanged() {
                        tvData.calculateSeenStatus();
                        if (MovieDataHolder.getInstance().getTvById(tvData.id) == null) {
                            tvData = MovieDataHolder.getInstance().addTv(tvData);
                            currentEpisode = tvData.seasons.get(currentSeasonIndex).episodes.get(currentEpisodeIndex);
                        }
                        displayChangingData();
                    }
                });
                dialog.show(getSupportFragmentManager(), SetMovieSeenStatusDialog.TAG);
                break;
            case R.id.action_addEpisodeToWatchlist:
                if (currentEpisode.isSeen()) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.are_you_sure)
                            .setMessage(R.string.episode_to_watchlist_dialog_message)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    currentEpisode.setSeen(false);
                                    tvData.setSeenStatus(TvData.SeenStatus.WATCHLIST);
                                    displayChangingData();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                } else {
                    currentEpisode.setSeen(false);
                    tvData.setSeenStatus(TvData.SeenStatus.WATCHLIST);
                    displayChangingData();
                }
                break;
            case R.id.action_rate:
                RateDialog rateDialog = new RateDialog();
                rateDialog.setListener(new RateDialog.OnRatedListener() {
                    @Override
                    public void onRated() {
                        displayChangingData();
                    }
                });
                rateDialog.setRateable(currentEpisode);
                rateDialog.show(getSupportFragmentManager(), RateDialog.TAG);
                break;
            default:
                break;
        }
        return true;
    }


}
