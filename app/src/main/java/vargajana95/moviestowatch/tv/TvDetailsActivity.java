package vargajana95.moviestowatch.tv;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Locale;

import vargajana95.moviestowatch.BaseDetailsActivity;
import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.RateDialog;
import vargajana95.moviestowatch.model.Episode;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.model.TvData;

public class TvDetailsActivity extends BaseDetailsActivity implements TvSeasonsFragment.TvDataProvider {

    public static final String TV_ID = "TV_ID";

    private interface BottomNavigationPage {
        int OVERVIEW = 0;
        int SEASONS = 1;
    }

    private int bottomNavigationPage;

    private BottomNavigationView bottomNavigationView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_overview:
                    if (bottomNavigationPage != BottomNavigationPage.OVERVIEW) {
                        bottomNavigationPage = BottomNavigationPage.OVERVIEW;
                        changeFragment(TvDetailsFragment.class);
                    }
                    return true;
                case R.id.navigation_seasons:
                    if (bottomNavigationPage != BottomNavigationPage.SEASONS) {
                        bottomNavigationPage = BottomNavigationPage.SEASONS;
                        changeFragment(TvSeasonsFragment.class);
                    }
                    return true;
            }
            return false;
        }
    };


    private ProgressBar pbSeen;
    private TextView tvSeenPercent;
    private TextView tvLastSeason;
    private TextView tvLastEpisode;
    private TextView tvWatchDate;
    private TextView tvReleaseDate;
    private TextView tvRuntime;
    private LinearLayout dateView;

    private ConstraintLayout clOnWatchlist;
    private Button btnWatch;

    private TvData tvData;
    private TextView tvGenres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_details);

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        bindActivity();


        tvGenres = findViewById(R.id.tvGenres);

        pbSeen = findViewById(R.id.progressBar);
        tvSeenPercent = findViewById(R.id.tvSeenPercent);
        tvLastSeason = findViewById(R.id.tvLastSeason);
        tvLastEpisode = findViewById(R.id.tvLastEpisode);
        tvWatchDate = findViewById(R.id.tvWatchDate);
        tvReleaseDate = findViewById(R.id.tvReleaseDate);
        tvRuntime = findViewById(R.id.tvRuntime);
        clOnWatchlist = findViewById(R.id.clOnWatchlist);
        btnWatch = findViewById(R.id.btnWatch);
        dateView = findViewById(R.id.dateView);

        tvGenres.setSelected(true);






        //TODO else
        int tvID=getIntent().getExtras().getInt(TV_ID, -1);
        if (tvID != -1) {
            //Movie is not in the MovieDataHolder, need to get info from api
            TvData tvToDisplay = MovieDataHolder.getInstance().getTvById(tvID);
            if (tvToDisplay != null) {
                displayTvData(tvToDisplay);
                //tvData = tvToDisplay;
            }
            else {
                tvToDisplay = MovieDataHolder.getInstance().getFromTvCacheById(tvID);
                if (tvToDisplay != null) {
                    displayTvData(tvToDisplay);
                    //tvData = tvToDisplay;
                }
                else {
                    //loadTvData(tvID);
                    finish();
                }
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        TvData newTvData = MovieDataHolder.getInstance().getTvById(tvData.id);
        if (newTvData != null) {
            tvData = newTvData;
        }
        invalidateOptionsMenu();
        displaySeenPanel();
    }

    @Override
    protected void displayHeader() {
        setPosterImage("https://image.tmdb.org/t/p/w500" + tvData.poster_path);

        setBackdropImage("https://image.tmdb.org/t/p/w500" + tvData.backdrop_path);

        tvGenres.setText(tvData.getGenresAsString());

        setCollapsingTitle(tvData.name);

        tvReleaseDate.setText(tvData.getAirDate());
        if (tvData.episode_run_time.size() > 0)
            tvRuntime.setText(getString(R.string.run_time, tvData.episode_run_time.get(0)));

    }

    @Override
    protected void displayInfo() {
            if (bottomNavigationPage == BottomNavigationPage.OVERVIEW)
                changeFragment(TvDetailsFragment.class);
            else if (bottomNavigationPage == BottomNavigationPage.SEASONS)
                changeFragment(TvSeasonsFragment.class);

    }


    protected void displayTvData(TvData receivedTvData) {
        invalidateOptionsMenu();

        tvData = receivedTvData;
        //MovieDataHolder.getInstance().addTvToCache(tvData);

        displayHeader();
        displaySeenPanel();
        displayInfo();
    }


    @Override
    protected void displaySeenPanel() {
        Episode episodeLastSeen = tvData.getLastSeenEpisode();

        if (tvData.getSeenStatus() == TvData.SeenStatus.SEEN_ALL_EPISODES || tvData.getSeenStatus() == TvData.SeenStatus.WATCHLIST) {
                btnWatch.setVisibility(View.GONE);
                clOnWatchlist.setVisibility(View.VISIBLE);
            if (episodeLastSeen != null) {
                tvLastSeason.setText(getString(R.string.last_season, episodeLastSeen.season_number));
                tvLastEpisode.setText(getString(R.string.last_episode, episodeLastSeen.episode_number));
                if (episodeLastSeen.getSeenDate() != null) {
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    tvWatchDate.setText(getString(R.string.seen_date, format1.format(episodeLastSeen.getSeenDate().getTime())));
                    dateView.setVisibility(View.VISIBLE);
                }
                else {
                    dateView.setVisibility(View.INVISIBLE);
                }
            } else {
                tvLastSeason.setText("-");
                tvLastEpisode.setText("");
                dateView.setVisibility(View.INVISIBLE);
            }
        } else {
            btnWatch.setVisibility(View.VISIBLE);
            clOnWatchlist.setVisibility(View.GONE);
        }

        int episodesSeenPercent = tvData.getSeenPercent();

        tvSeenPercent.setText(getString(R.string.seen_percent, episodesSeenPercent));
        pbSeen.setProgress(episodesSeenPercent);
    }

    private void changeFragment(Class fragmentClass) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        /*if (bottomNavigationPage == BottomNavigationPage.OVERVIEW)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        else if (bottomNavigationPage == BottomNavigationPage.SEASONS)
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);*/
        ft.replace(R.id.fragment, fragment).commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tv_details, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (tvData != null) {
            if (tvData.getSeenStatus() == TvData.SeenStatus.WATCHLIST){
                menu.findItem(R.id.menu_add_to_watchlist).setVisible(false);
                menu.findItem(R.id.menu_add_to_watchlist).setTitle(R.string.add_to_watchlist);
                menu.findItem(R.id.menu_seen_it).setVisible(true);
                menu.findItem(R.id.action_remove).setVisible(true);
                menu.findItem(R.id.action_rate).setVisible(true);
            }
            else if (tvData.getSeenStatus() == TvData.SeenStatus.NOT_ON_LIST){
                menu.findItem(R.id.menu_add_to_watchlist).setVisible(true);
                menu.findItem(R.id.menu_add_to_watchlist).setTitle(R.string.add_to_watchlist);
                menu.findItem(R.id.menu_seen_it).setVisible(true);
                menu.findItem(R.id.action_remove).setVisible(false);
                menu.findItem(R.id.action_rate).setVisible(false);
            }
            else if (tvData.getSeenStatus() == TvData.SeenStatus.SEEN_ALL_EPISODES){
                menu.findItem(R.id.menu_add_to_watchlist).setVisible(true);
                menu.findItem(R.id.menu_add_to_watchlist).setTitle(R.string.add_back_to_watchlist);
                menu.findItem(R.id.menu_seen_it).setVisible(false);
                menu.findItem(R.id.action_remove).setVisible(true);
                menu.findItem(R.id.action_rate).setVisible(true);
            }

        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_to_watchlist:

                if (tvData.getSeenStatus() == TvData.SeenStatus.SEEN_ALL_EPISODES) {
                    new AlertDialog.Builder(this)
                            .setTitle("Are you sure?")
                            .setMessage(R.string.tv_to_watchlist_dialog_message)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    tvData.setSeenStatus(TvData.SeenStatus.WATCHLIST);
                                    tvData.setAllEpisodesSeen(false);
                                    displayChangingData();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                } else {
                    tvData.setSeenStatus(TvData.SeenStatus.WATCHLIST);
                    if (MovieDataHolder.getInstance().getTvById(tvData.id) == null) {
                        tvData = MovieDataHolder.getInstance().addTv(tvData);
                    }
                    displayChangingData();
                }
                break;
            case R.id.menu_seen_it:
                //tvData.setSeenStatus(TvData.SeenStatus.SEEN_ALL_EPISODES);
                //tvData.setAllEpisodesSeen(true);
                SelectSeenSeasonsDialog dialog = new SelectSeenSeasonsDialog();
                dialog.setTvData(tvData);
                dialog.setListener(new SelectSeenSeasonsDialog.OnTvChangedListener() {
                    @Override
                    public void onTvChanged(TvData newTvData) {
                        tvData = newTvData;
                        tvData.calculateSeenStatus();
                        displayChangingData();
                    }
                });
                dialog.show(getSupportFragmentManager(), SelectSeenSeasonsDialog.TAG);
                break;
            case R.id.action_remove:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.delete_tvshow_title)
                        .setMessage(getString(R.string.with_questionmark, tvData.name))
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tvData.setSeenStatus(MovieData.SeenStatus.NOT_ON_LIST);
                                tvData.setPersonalRating(0);
                                tvData.setAllEpisodesSeen(false);
                                tvData = MovieDataHolder.getInstance().removeTv(tvData);
                                displayChangingData();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();

                break;
            case R.id.action_rate:
                RateDialog rateDialog = new RateDialog();
                rateDialog.setListener(new RateDialog.OnRatedListener() {
                    @Override
                    public void onRated() {
                        displayChangingData();
                    }
                });
                rateDialog.setRateable(tvData);
                rateDialog.show(getSupportFragmentManager(), RateDialog.TAG);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public TvData getTvData() {
        return tvData;
    }
}
