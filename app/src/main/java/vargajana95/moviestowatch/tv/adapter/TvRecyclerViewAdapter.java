package vargajana95.moviestowatch.tv.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.MovieDataHolder;
import vargajana95.moviestowatch.model.TvData;
import vargajana95.moviestowatch.tv.TvDetailsActivity;

/**
 * Created by Varga János on 2018. 02. 24..
 */

public class TvRecyclerViewAdapter extends RecyclerView.Adapter<TvRecyclerViewAdapter.ViewHolder> {

    private List<TvData> tvDatas;
    private Context context;

    public TvRecyclerViewAdapter(Context context) {
        this.context = context;
        tvDatas = new ArrayList<>();//MovieDataHolder.getInstance().getMovies();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_layout, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //holder.mTodo = todos.get(position);

        holder.tvData = tvDatas.get(position);

        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500" +
                        holder.tvData.poster_path)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_movie))
                .into(holder.ivPoster);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TvDetailsActivity.class);
                intent.putExtra(TvDetailsActivity.TV_ID, holder.tvData.id);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                PopupMenu popup = new PopupMenu(v.getContext(), v);
                popup.inflate(R.menu.menu_long_click);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (R.id.popup_remove == item.getItemId()) {
                            new AlertDialog.Builder(context)
                                    .setTitle(R.string.delete_tvshow_title)
                                    .setMessage(context.getString(R.string.with_questionmark, holder.tvData.name))
                                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            tvDatas.remove(holder.tvData);
                                            MovieDataHolder.getInstance().removeTv(holder.tvData);
                                            notifyItemRemoved(holder.getAdapterPosition());
                                        }
                                    })
                                    .setNegativeButton(R.string.no, null)
                                    .show();
                        }
                        return false;
                    }
                });
                popup.show();
                return false;
            }
        });




    }

    public void deleteItem(int position) {

        notifyDataSetChanged();
    }


    public void addItem(TvData tv) {
        tvDatas.add(tv);
        notifyDataSetChanged();
    }



    @Override
    public int getItemCount() {
        return tvDatas.size();
    }

    public void setList(List<TvData> list) {
        this.tvDatas = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ImageView ivPoster;

        public TvData tvData;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivPoster = view.findViewById(R.id.ivPoster);
            /*title = (TextView) view.findViewById(R.id.textViewTitle);
            dueDate = (TextView) view.findViewById(R.id.textViewDueDate);
            priority = (ImageView) view.findViewById(R.id.imageViewPriority);*/
        }
    }
}
