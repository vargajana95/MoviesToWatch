package vargajana95.moviestowatch.tv.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.Season;
import vargajana95.moviestowatch.model.TvData;
import vargajana95.moviestowatch.tv.EpisodeDetailsActivity;

/**
 * Created by Varga János on 2018. 02. 23..
 */

//TODO use custom recycler view instead?
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> listDataChild;
    private TvData tvData;

    public ExpandableListAdapter(Context context, TvData tvData) {
        this.context = context;
        this.tvData = tvData;

        prepareListData();
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        for (int i = 0; i < tvData.number_of_seasons; i++) {
            Season season = tvData.seasons.get(i);
            listDataHeader.add(season.name);

            List<String> episodeList = new ArrayList<>();
            for (int j = 0; j < season.episodes.size(); j++) {
                episodeList.add(season.episodes.get(j).name);
            }

            listDataChild.put(season.name, episodeList);
        }
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.episode_list_item, null);
        }

        final TextView txtListChild = convertView.findViewById(R.id.lblListItem);


        txtListChild.setText(childText);
        txtListChild.setSelected(true);

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EpisodeDetailsActivity.class);
                intent.putExtra(EpisodeDetailsActivity.TV_ID, tvData.id);
                intent.putExtra(EpisodeDetailsActivity.EPISODE_NUMBER, childPosition);
                intent.putExtra(EpisodeDetailsActivity.SEASON_NUMBER, groupPosition);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
        TextView tvEpisode =  convertView.findViewById(R.id.tvEpisode);
        tvEpisode.setText(context.getString(R.string.episode_num, childPosition+1));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.season_list_group, null);
        }

        TextView lblListHeader =  convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        ImageView ivIndicator = convertView.findViewById(R.id.ivIndicator);

        if (isExpanded)
            ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24px));
        else
            ivIndicator.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_keyboard_arrow_right_black_24px));



        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
