package vargajana95.moviestowatch.tv;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.MovieData;
import vargajana95.moviestowatch.model.TvData;

/**
 * A placeholder fragment containing a simple view.
 */
public class TvDetailsFragment extends Fragment {

    public static String TAG = "TvDetailsFragment";

    private TextView tvOverView;
    private TextView tvActors;
    private TextView tvCreators;
    private TextView tvRating;
    private TextView tvPersonalScore;
    private TextView tvImdbLink;
    private TextView tvTmdbLink;
    private LinearLayout personalScoreLayout;

    private TvData tvData;
    private TvSeasonsFragment.TvDataProvider tvDataProvider;

    public TvDetailsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof TvSeasonsFragment.TvDataProvider) {
            tvDataProvider = (TvSeasonsFragment.TvDataProvider) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement TvDataProvider interface!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        tvData = tvDataProvider.getTvData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv_details, container, false);


        tvData = tvDataProvider.getTvData();
        // get the listview

        tvOverView = view.findViewById(R.id.tvOverView);
        tvActors = view.findViewById(R.id.tvActors);
        tvCreators = view.findViewById(R.id.tvCreators);
        tvRating = view.findViewById(R.id.tvRating);
        tvPersonalScore = view.findViewById(R.id.tvPersonalScore);
        tvImdbLink = view.findViewById(R.id.tvImdbLink);
        tvTmdbLink = view.findViewById(R.id.tvTmdbLink);
        personalScoreLayout = view.findViewById(R.id.personalScoreLayout);

        TvData tv = tvDataProvider.getTvData();
        if (tv != null)
            displayTvData(tv);

        return view;
    }

    private void displayTvData(TvData tvData) {
        tvOverView.setText(tvData.overview);
        tvActors.setText(tvData.getTopActorsAsString(3));
        tvCreators.setText(tvData.getCreatorsAsString(3));
        tvRating.setText(getContext().getString(R.string.string_rating, tvData.vote_average));
        if (tvData.getPersonalRating() == 0 || tvData.getSeenStatus() == MovieData.SeenStatus.NOT_ON_LIST) {
            personalScoreLayout.setVisibility(View.GONE);
        }
        else {
            personalScoreLayout.setVisibility(View.VISIBLE);
            tvPersonalScore.setText(getContext().getString(R.string.string_rating, tvData.getPersonalRating()));
        }

        tvImdbLink.setText(tvData.getImdbLink());
        tvTmdbLink.setText(tvData.getTmdbLink());

    }

}

