package vargajana95.moviestowatch.tv;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.HashMap;
import java.util.List;

import vargajana95.moviestowatch.R;
import vargajana95.moviestowatch.model.TvData;
import vargajana95.moviestowatch.tv.adapter.ExpandableListAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class TvSeasonsFragment extends Fragment {
    public static String TAG = "TvSeasonsFragment";

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    private TvData tvData;
    private TvDataProvider tvDataProvider;

    public TvSeasonsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() instanceof TvDataProvider) {
            tvDataProvider = (TvDataProvider) getActivity();
        } else {
            throw new RuntimeException(
                    "Activity must implement TvDataProvider interface!");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        tvData = tvDataProvider.getTvData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seasons_details, container, false);


        tvData = tvDataProvider.getTvData();
        // get the listview
        expListView = view.findViewById(R.id.lvExp);

        // preparing list data

        listAdapter = new ExpandableListAdapter(getContext(), tvData);

        // setting list adapter
        expListView.setAdapter(listAdapter);
        return view;
    }


    public interface TvDataProvider {
        TvData getTvData();
    }
}
